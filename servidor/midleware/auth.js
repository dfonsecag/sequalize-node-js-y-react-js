const { response } = require('express');
const jwt = require('jsonwebtoken');

module.exports = function(req, res=response, next){
    // Leer el token del header
    const token = req.header('x-auth-token');

    //Revisar si no hay token
    if(!token){
        return res.status(400).json({msg: 'No hay token, permiso no valido'})
    }

    //Validar el token
    try {
        const cifrado = jwt.verify(token, process.env.SECRETORPRIVATEKEY);
        req.usuario = cifrado;
        next();

    } catch (error) {
        res.status(401).json({msg: 'Token no valido'})
    }

}