const bcryptjs = require('bcryptjs');
var db = require("../models");
const Usuario = db.User;
const Tarea = db.Task;
const errorDb = "Algun error a la hora de conectar a la base de datos";

// Create and Save a new Tutorial
exports.create = async(req, res) => {
  
  // Create a Usuario
  const usuario = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    bio: req.body.bio,
    password: req.body.password,
    email:req.body.email
  };
  //Encryptar contrasena
  const salt = bcryptjs.genSaltSync();
  usuario.password = bcryptjs.hashSync(req.body.password, salt);
  
  try {
    const data = await Usuario.create(usuario);
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
 
};

// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.iLike]: `%${title}%` } } : null;

  Usuario.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || errorDb,
      });
    });
};
// Find a single Tutorial with an id
exports.findOne = async (req, res) => {
  const id = req.params.id;

  try {
    usuarioTareas = await Usuario.findOne({
      attributes: ["id", "first_name", "last_name"],
      where: {
        id,
      },
      include: [
        {
          model: Tarea,
          attributes: ["id", "title"],
        },
      ],
    });
    if (!usuarioTareas) {
      res.send({ msg: `Usuario ${id} no existe` });
    }
    res.send(usuarioTareas);
  } catch (x) {
    res.status(500).send({
      message: x.message || errorDb,
    });
  }
};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Usuario.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: `Usuario actualizado correctamente : ${num}`,
        });
      } else {
        res.send({
          message: `No se encontro el usuario con el id: ${id}`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error actualizando Usuario con id=" + id,
      });
    });
};

// Delete a Tutorial with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Usuario.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Usuario se elimino exitosamente",
        });
      } else {
        res.send({
          message: `No se encontro el tutorial=${id}`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Tutorial with id=" + id,
      });
    });
};


