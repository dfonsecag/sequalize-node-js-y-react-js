var db = require("../models");
const Tarea = db.Task;
const User = db.User;
const Herramienta= db.Herramienta;
const errorDb = "Algun error a la hora de conectar a la base de datos";

// Create and Save a new Tutorial
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "No se encontro el titulo de la tarea",
    });
    return;
  }

  // Create a tarea
  const tarea = {
    title: req.body.title,
    UserId: req.usuario.id,
  };

  try {
    const tareaInser = await Tarea.create(tarea);
    res.send(tareaInser);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};


exports.findAll = async(req, res) => {
  

  try {
    const tareas = await Tarea.findAll({
      attributes: ["id", "title"],
      where: {
        UserId: req.usuario.id
      },
      // include: [
      //   {
      //     model: User,
      //     attributes: ["id", "first_name", "last_name"],
      //   },
      // ],
    })
    res.send(tareas);
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }   
};
// Find a single Tutorial with an id
exports.findOne = async (req, res) => {
  const id = req.params.id;

  try {
    tarea = await   Tarea.findOne({
      attributes: ["id", "title"],
      where: {
          id
      },
      include: [
        {
          model: User,
          attributes: ["id", "first_name", "last_name"],
        },
        {
          model: Herramienta,
          attributes: ["id", "title","description"]          
        },
      ],
    })

    res.send(tarea);
  } catch (x) {
    res.status(500).send({
          message: x.message || errorDb,
        });
  }
};

// Update a Tutorial by the id in the request
exports.update = async(req, res) => {
  const id = req.params.id;
  try {
    tareaAct = await Tarea.update(req.body, {
      where: { id: id },
    })
    if (tareaAct == 1) {
      res.send({
        message: `Tarea actualizado correctamente : ${tareaAct}`,
      });
    }
      res.send({
        message: `No se encontro la tarea con el id: ${id}`,
      });
    
  } catch (error) {
    res.status(500).send({
      message: "Error Servidor",
    });
  }
}
// Delete a Tutorial with the specified id in the request
exports.delete = async(req, res) => {
  const id = req.params.id;

  try {
    num = await  Tarea.destroy({where: { id: id }});
    if (num == 1) {
      res.send({
        message: "Tarea se elimino exitosamente",
      });
    } else {
      res.send({
        message: `No se encontro la tarea id: ${id}`,
      });
    }
  } catch (error) {
    res.status(500).send({
      message: "Error en el servidor",
    });
  }   
};
