module.exports = app => {
    const herramientas = require("../controllers/herramienta");
  
    var router = require("express").Router();
  
    // Create a new herramientas
    router.post("/", herramientas.create);
  
    // Retrieve all herramientas
    router.get("/", herramientas.findAll);

    // Retrieve a single herramientas with id
    router.get("/:id", herramientas.findOne);
  
    // Update a herramientas with id
    router.put("/:id", herramientas.update);
  
    // Delete a herramientas with id
    router.delete("/:id", herramientas.delete);
  
    
  
    app.use('/api/herramientas', router);
  };