module.exports = app => {
    const tareas = require("../controllers/task");
    const auth = require("../midleware/auth");
  
    var router = require("express").Router();
  
    // Create a new tareas
    router.post("/",auth, tareas.create);
  
    // Retrieve all tareas
    router.get("/",auth ,tareas.findAll);

    // Retrieve a single tareas with id
    router.get("/:id", tareas.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", tareas.update);
  
    // Delete a tareas with id
    router.delete("/:id", tareas.delete);
  
    
  
    app.use('/api/tareas', router);
  };