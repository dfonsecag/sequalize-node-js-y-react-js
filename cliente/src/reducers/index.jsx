import { combineReducers } from "redux";

import loginReducer from "./loginReducer";
import tareaReducer from "./tareaReducer";
import herramientaReducer from "./herramientaReducer";
import alertaReducer from "./alertaReducer";
import usuarioReducer from "./usuarioReducer";

export default combineReducers({
  login: loginReducer,
  tareas: tareaReducer,
  herramientas: herramientaReducer,
  alertas: alertaReducer,
  usuario: usuarioReducer
});
