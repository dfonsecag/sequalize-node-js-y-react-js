import {
  CREAR_USUARIO,
  CREAR_USUARIO_EXITO,
  CREAR_USUARIO_ERROR,
} from "../types";
// cada reducer tiene su propio state
const initialState = {
  loading: false,
  error: false,
  exito:false
};
export default function (state = initialState, action) {
  switch (action.type) {
    case CREAR_USUARIO:
      return {
        ...state,
        loading: true,
      };
    case CREAR_USUARIO_EXITO:
      return {
        ...state,
        loading: false,
        error: false,
        exito:true
      };
    case CREAR_USUARIO_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        exito:false
      };
    default:
      return state;
  }
}
