import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// REDUX
import { Provider } from "react-redux";
import store from "./store";

import Login from "./components/auth/Login";
import Tarea from "./components/Tareas";
import NuevaCuenta from "./components/auth/NuevaCuenta";



function App() {
  return (
  
            <Router>
              <Provider store={store}>
              <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/tareas" component={Tarea} />
                <Route exact path="/nueva_cuenta" component={NuevaCuenta} />
                {/* <RutaPrivada exact path="/proyectos" component={Proyectos} /> */}
              </Switch>
              </Provider>
            </Router>
        
  );
}

export default App;
