import React, { Fragment, useEffect } from "react";
import {  useSelector, useDispatch } from "react-redux";
import { mostrarHerramienteAction } from "../../actions/herramientaAction";
import { eliminarTareaAction } from "../../actions/tareaAction";

import Herramienta from './Herramienta'

const ListadoHerramientas = () => {
  const dispatch = useDispatch();
  const tareaSeleccionada = useSelector((state) => state.tareas.tareaSeleccionada);
  const herramientas = useSelector((state) => state.herramientas.herramientas);
 
   const eliminartarea = (id) => dispatch(eliminarTareaAction(id));
 
  // Llamar funcion para obtener los proyectos
  useEffect(() => {
    // Consultar la api
    if(tareaSeleccionada !== null){
    const cargarHerramientas = (tarea) => dispatch(mostrarHerramienteAction(tarea));
    cargarHerramientas(tareaSeleccionada.id);
    }
  }, [tareaSeleccionada]);



  //Si no hay proyecto seleccionado
  if (tareaSeleccionada === null) return <h2>Selecciona un proyecto</h2>;

//   // array destructurin para extraer el proyecto
//   const [proyectoActual] = proyecto;

  return (
    <Fragment>
      <h2>Proyecto: {tareaSeleccionada.title}</h2>
        
      <ul className="listado-tareas">
        {herramientas.length === 0 ? (
          <li className="tarea">No hay herramientas...</li>
        ) : (
              herramientas.map((herramienta) => (
             
                <Herramienta key={herramienta.id} herramienta={herramienta} />
              
            ))
          
        )}
      </ul>

      <button
        type="button"
        className="btn btn-eliminar"
        onClick={() => eliminartarea(tareaSeleccionada.id)}
      >
        Eliminar Tarea &times;
      </button>
    </Fragment>
  );
};

export default ListadoHerramientas;
