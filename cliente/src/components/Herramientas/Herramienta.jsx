import React from "react";
import { useDispatch } from "react-redux";
import { eliminarHerramientaAction, herramientaSeleccionada } from "../../actions/herramientaAction";

const Herramienta = ({ herramienta }) => {
  const dispatch = useDispatch();

  // Funcion que se ejecuta cuando el usuario toca el boton de eliminar herramienta
  const eliminarherramienta = (id) => dispatch(eliminarHerramientaAction(id));
  // Funcion que se ejecuta cuando el usuario va comenzar edicion de una herramienta
  const seleccionarherramienta = (herramienta) => dispatch(herramientaSeleccionada(herramienta));

  return (
    <li className="tarea sombra">
      <p>{herramienta.title}</p>
      <div className="acciones">
        <button
          type="button"
          className="btn btn-primario"
           onClick={() => seleccionarherramienta(herramienta)}
        >
          Editar
        </button>
        <button
          type="button"
          className="btn btn-sencundario"
          onClick={() => eliminarherramienta(herramienta.id)}
        >
          Eliminar
        </button>
      </div>
    </li>
  );
};

export default Herramienta;
