import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../actions/loginAction";
import {
  mostrarAlertaAction,
  ocultarAlertaAction,
} from "../../actions/alertaAction";

const Login = (props) => {

  const dispatch = useDispatch();
  const autenticado = useSelector((state) => state.login.login);
  const alerta = useSelector((state) => state.alertas);

  //State para iniciar sesion
  const [usuario, guardarUsuario] = useState({
    email: "",
    password: "",
  });

   // En caso de que el usuario digite un password o usuario no exista
   useEffect(() => {
    if (autenticado === true) {
      props.history.push("/tareas");
    }
    
    // eslint-disable-next-line
  }, [autenticado, props.history]);

  const login = (usuario) =>
  dispatch(loginAction(usuario));
  //extraer los datos
  const { email, password } = usuario;
  const onChange = (e) => {
    guardarUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };
  //enviar los datos del formulario submit
  const onSubmit = (e) => {
     e.preventDefault();

    //validacion que no hayan campos vacios
    if (email.trim() === "" || password.trim() === "") {
     //mostrarAlerta("Todos los campos son obligatorio", "mensaje error");
     dispatch(mostrarAlertaAction("Todos los campos son obligatorios"));
     return;
    }

    //pasar al action
    login({ email, password });
    dispatch(ocultarAlertaAction());
  };

  return (
    <div className="form-usuario">
      <div className="contenedor-form sombra-dark">
        <h1>Iniciar Sesion</h1>
        {alerta.alerta ? (
          <p className="mensaje error">{alerta.mensaje}</p>
        ) : null}

        <form onSubmit={onSubmit}>
          <div className="campo-form">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              value={email}
              placeholder="Tu email"
              onChange={onChange}
            />
          </div>

          <div className="campo-form">
            <label htmlFor="password">Contrasena</label>
            <input
              type="password"
              id="password"
              name="password"
              value={password}
              placeholder="Tu contrasena"
              onChange={onChange}
            />
          </div>

          <div className="campo-form">
            <input
              type="submit"
              className="btn btn-primario btn-block"
              value="Inisiar Sesion"
            />
          </div>
        </form>

        <Link to={"/nueva_cuenta"} className="enlace-cuenta">
          {" "}
          Obtener Cuenta{" "}
        </Link>
      </div>
    </div>
  );
};

export default Login;
