import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  mostrarAlertaAction,
  ocultarAlertaAction,
} from "../../actions/alertaAction";
import { crearUsuarioAction } from "../../actions/usuarioAction";

const NuevaCuenta = (props) => {
  const dispatch = useDispatch();
  const alerta = useSelector((state) => state.alertas);
  const usuarioCreadoExistoso = useSelector((state) => state.usuario.exito);
  const registrarUsuario = (usuario) => dispatch(crearUsuarioAction(usuario));

  useEffect(() => {
    if (usuarioCreadoExistoso) {
      props.history.push("/");
    }
  }, [usuarioCreadoExistoso]);

  //State para iniciar sesion
  const [usuario, guardarUsuario] = useState({
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    confirmar: "",
  });
  //extraer los datos
  const { first_name, last_name, email, password, confirmar } = usuario;

  const onChange = (e) => {
    guardarUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };
  //enviar los datos del formulario submit
  const onSubmit = (e) => {
    e.preventDefault();

    //validacion que no hayan campos vacios
    if (
      first_name.trim() === "" ||
      last_name.trim() === "" ||
      email.trim() === "" ||
      password.trim() === "" ||
      confirmar.trim() === ""
    ) {
      dispatch(mostrarAlertaAction("Todos los campos son obligatorios"));
      return;
    }

    // password minimo de 6 caracteres
    if (password.length < 6) {
      dispatch(
        mostrarAlertaAction("El password debe ser al menos 6 caracteres")
      );
      return;
    }

    //revisar que los dos password sean iguales
    if (password !== confirmar) {
      dispatch(mostrarAlertaAction("Las contrasena no son iguales"));
      return;
    }

    //pasar al action
    registrarUsuario({ first_name, last_name, email, password });
    dispatch(ocultarAlertaAction());
  };

  return (
    <div className="form-usuario">
      <div className="contenedor-form sombra-dark">
        <h1>Registrar una cuenta</h1>
        {alerta.alerta ? (
          <p className="mensaje error">{alerta.mensaje}</p>
        ) : null}

        <form onSubmit={onSubmit}>
          <div className="campo-form">
            <label htmlFor="first_name">Nombre</label>
            <input
              type="text"
              id="first_name"
              name="first_name"
              value={first_name}
              placeholder="Su nombre"
              onChange={onChange}
            />
          </div>
          <div className="campo-form">
            <label htmlFor="last_name">Apellidos</label>
            <input
              type="text"
              id="last_name"
              name="last_name"
              value={last_name}
              placeholder="Sus apellidos"
              onChange={onChange}
            />
          </div>

          <div className="campo-form">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              value={email}
              placeholder="Tu email"
              onChange={onChange}
            />
          </div>

          <div className="campo-form">
            <label htmlFor="password">Contrasena</label>
            <input
              type="password"
              id="password"
              name="password"
              value={password}
              placeholder="Tu contrasena"
              onChange={onChange}
            />
          </div>

          <div className="campo-form">
            <label htmlFor="confirmar">Confirmar Password</label>
            <input
              type="password"
              id="confirmar"
              name="confirmar"
              value={confirmar}
              placeholder="Repite tu contrasena"
              onChange={onChange}
            />
          </div>

          <div className="campo-form">
            <input
              type="submit"
              className="btn btn-primario btn-block"
              value="Registrar"
            />
          </div>
        </form>

        <Link to={"/"} className="enlace-cuenta">
          {" "}
          Volver a iniciar sesion{" "}
        </Link>
      </div>
    </div>
  );
};

export default NuevaCuenta;
