import React, { useEffect } from "react";
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux";
import { cerrarSesion } from "../../actions/loginAction";

const Barra = (props) => {
  const history = useHistory();
  const usuario = useSelector((state) => state.login.usuario);
  const dispatch = useDispatch();
  const cerrarSesionUsuario = () => {
    dispatch(cerrarSesion());
    history.push("/");
  };

  return (
    <header className="app-header">
      {usuario ? (
        <p className="nombre-usuario">
          Hola <span>{`${usuario.first_name} ${usuario.last_name}`}</span>
        </p>
      ) : null}
      
      <nav className="nav-principal">
        <button
          className="btn cerrar-sesion"
          onClick={() => cerrarSesionUsuario()}
        >
          Cerrar Sesion
        </button>
      </nav>
    </header>
  );
};

export default Barra;
