import React from 'react'

import NuevaTarea from '../tareas/NuevaTarea';
import ListadoTareas from "../tareas/ListadoTarea";

const Sidebar = () => {
    return ( 
        <aside>
            <h1> SVM <span>Tareas</span></h1>

            <NuevaTarea/>

            <div className="proyectos">
                <h2>Tus tareas</h2>

                <ListadoTareas/>

            </div>
        </aside>
     );
}
 
export default Sidebar;