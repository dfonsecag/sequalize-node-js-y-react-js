import React, { useEffect } from "react";
import Tarea from "./Tarea";

import { useDispatch, useSelector } from "react-redux";
import { mostrarTareaAction } from "../../actions/tareaAction";

const ListadoTareas = () => {
  
  const dispatch = useDispatch();
 
  // Llamar funcion para obtener los proyectos
  useEffect(() => {
    // Consultar la api
    const cargarTareas = () => dispatch(mostrarTareaAction());
    cargarTareas();
  }, []);

  const tareas = useSelector((state) => state.tareas.tareas);

  //revisar si proyectos tiene contenido
  if (tareas.length === 0) {
    return <p>No hay tareas, comience creando una</p>;
  }

  return (
    <ul className="listado-proyectos">
      {/* {alerta ? <p className={`${alerta.categoria}`}>{alerta.msg}</p> : null} */}
      {
      tareas.length === 0 ? "No hay productos por mostrar"
      :tareas.map((tarea) => (
        <Tarea key={tarea.id} tarea={tarea} />
      ))}
    </ul>
  );
};

export default ListadoTareas;
