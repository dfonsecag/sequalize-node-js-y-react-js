import React from "react";
import { useDispatch,  } from "react-redux";
import { tareaSeleccionada } from "../../actions/tareaAction";
import { herramientadeseleccionar } from "../../actions/herramientaAction";

const Tarea = ({ tarea }) => {
  const dispatch = useDispatch();
  
  const seleccionarTarea = (tarea)=>{
    dispatch(tareaSeleccionada(tarea));
    dispatch(herramientadeseleccionar());
  }

  return (
    <li>
      <button type="button" 
      className="btn btn-blank"
      onClick={() => seleccionarTarea(tarea)}
      >
        {tarea.title}
      </button>
    </li>
  );
};

export default Tarea;