import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { autenticadoAction } from "../actions/loginAction";

import Barra from "../components/Layout/Barra";
import Sidebar from "./Layout/Sidebar";
import ListadoHerramientas from "./Herramientas/ListadoHerramientas";
import FormHerramienta from "./Herramientas/FormHerramienta";

const Tarea = (props) => {
  const dispatch = useDispatch();
  const autenticado = useSelector((state) => state.login.login);
  const proyecto = useSelector((state) => state.tareas.tareaSeleccionada);
  // En caso de que el usuario digite un password o usuario no exista
  useEffect(() => {
    dispatch(autenticadoAction());
    if (autenticado !== true) {
      props.history.push("/");
    }

    // eslint-disable-next-line
  }, [autenticado, props.history]);
  return (
    <div className="contenedor-app">
      <Sidebar />

      <div className="seccion-principal">
        <Barra props={props}/>

        <main>
          {proyecto ? <FormHerramienta /> : null}
          

          <div className="contenedor-tareas">
            <ListadoHerramientas />
          </div>
        </main>
      </div>
    </div>
  );
};

export default Tarea;
